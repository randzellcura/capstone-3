CAPSTONE 3 : Randzel Ivan L. Cura

Minimum Requirements

● Register Page (👍)
● Login Page (👍)
● Products Catalog Page (👍)
○ Retrieve all active products (👍)
○ Retrieve single product (👍)
● Admin Dashboard
○ Create Product (👍)
○ Retrieve all products (👍)
○ Update Product information (👍)
○ Deactivate/reactivate product (👍)
● Checkout Order
○ Non-admin User checkout (Create Order) (❌)

Other Requirements
● A fully-functioning Navbar with proper dynamic rendering (Register/Login links
for users not logged in, Logout link for users who are, etc) (👍)
● App must be single-page and utilize proper routing (no navigating to another
page/reloading) (👍)
● Registration/Login pages must be inaccessible to users who are logged-in (👍)
● Apart from users who are not logged-in, Admin must not be able to add
products to their cart (👍)
● Do not create a website other than the required e-commerce app (👍)
● Do not use templates found in other sites or existing premade NPM packages
that replicate a required feature (👍)

Stretch Goals
Full responsiveness across mobile/tablet/desktop screen sizes (50/50 Responsiveness)
● Product images (👍)
● A hot products/featured products section (❌)
● View User Details (Profile) - You can change their password (👍)
● Setting a user as an admin (👍)
● Add to Cart Feature (👍)
● Order History (❌)
○ Retrieve a list of all orders made by user (👍)
○ Admin feature to retrieve a list of all orders made by all users (👍)
● A Cart View page with the following features:
○ Show all items the user has added to their cart (and their quantities) (👍)
○ Change product quantities (👍)
○ Remove products from cart (👍)
○ Subtotal for each item (👍)
○ Total price for all items (👍)
○ A working checkout button/functionality. (❌)
○ When the user checks their cart out, redirect them to either the homepage or the
Order History page (❌)
