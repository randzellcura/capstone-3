
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Helmet } from 'react-helmet-async';
import '../index.css';
import YoutubeEmbed from '../components/YoutubeEmbed';

function HomeScreen() {

  return (
    <div>
      <Helmet>
        <title>Boosted</title>
      </Helmet>
      <div>
        <h2>Tired of losing every game?</h2>
        <Row>
          <Col sm={12} md={12} lg={6}>
            <img src='./images/valorant-defeat.png' className='hero-banner' alt='hero-banner'></img>
          </Col>
          <Col>
            <p>Well if you are, I am sure you are tired of getting bonobo teammates who can't get a single kill even after round twelve. Worry not my tryhard friend I got the solution for you 😏! </p>
            <p>You don't need teammates when you got SKINS! You know the saying skins for the win? Well guess what it's true, skins gives buffs like;</p>
            <ul>
              <li>x2 accuracy</li>
              <li>Clutch genes</li>
              <li>Map awareness for days!!!</li>
              <li>Wall hacks</li>
              <li>AND MANY MORE!!!</li>
            </ul>
          </Col>
        </Row>
      </div>

      <div className='pt-4'>
        <h4>Montage feat SKINS</h4>
        <YoutubeEmbed embedId="fAyUxeDzKlI" />
      </div>
    </div>
  );
}
export default HomeScreen;
